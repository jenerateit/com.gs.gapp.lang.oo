package com.gs.gapp.metamodel.oo;


import org.junit.Assert;
import org.junit.Test;

public class TypeTest {

	private final Type t1 = new Type("t1", true);
	private final Type t2 = new Type("t2", true);
	
	@Test
	public void addOperation() {
		Type t = new Type("t", Modifier.getModifier(Modifier.PROTECTED, Modifier.FINAL), false);

		Assert.assertNotNull(t.getOperations());
		Assert.assertTrue(t.getOperations().isEmpty());
		
		Operation<Type> o = new Operation<Type>("o", Modifier.PUBLIC, t);
		Assert.assertTrue(o.addParameter(new Parameter("p1", t1)));
		Assert.assertTrue(o.addParameter(new Parameter("p2", t2)));
		Assert.assertTrue(t.addOperation(o));
		Assert.assertEquals(t.getOperations().size(), 1);
		
		o = new Operation<Type>("o2", Modifier.PUBLIC, t);
		Assert.assertTrue(o.addParameter(new Parameter("p1", t1)));
		Assert.assertTrue(o.addParameter(new Parameter("p2", t2)));
		Assert.assertTrue(t.addOperation(o));
		Assert.assertEquals(t.getOperations().size(), 2);
	}
	

	@Test
	public void addOperationTwice() {
		Type t = new Type("t", Modifier.getModifier(Modifier.PROTECTED, Modifier.FINAL), false);

		Operation<Type> o = new Operation<Type>("o", Modifier.PUBLIC, t);
		Assert.assertTrue(o.addParameter(new Parameter("p1", t1)));
		Assert.assertTrue(o.addParameter(new Parameter("p2", t2)));
		Assert.assertTrue(t.addOperation(o));
		
		o = new Operation<Type>("o", Modifier.PUBLIC, t);
		Assert.assertTrue(o.addParameter(new Parameter("p1", t1)));
		Assert.assertTrue(o.addParameter(new Parameter("p2", t2)));
		Assert.assertFalse(t.addOperation(o));
	}
	

	@Test
	public void getOperationsByName() {
		Type t = new Type("t", Modifier.getModifier(Modifier.PROTECTED, Modifier.FINAL), false);

		Operation<Type> o = new Operation<Type>("o", Modifier.PUBLIC, t);
		Assert.assertTrue(o.addParameter(new Parameter("p1", t1)));
		Assert.assertTrue(o.addParameter(new Parameter("p2", t2)));
		Assert.assertTrue(t.addOperation(o));

		o = new Operation<Type>("o", Modifier.PUBLIC, t);
		Assert.assertTrue(o.addParameter(new Parameter("p2", t2)));
		Assert.assertTrue(t.addOperation(o));

		o = new Operation<Type>("o", Modifier.PUBLIC, t);
		Assert.assertTrue(o.addParameter(new Parameter("p1", t1)));
		Assert.assertTrue(t.addOperation(o));
		
		o = new Operation<Type>("o1", Modifier.PUBLIC, t);
		Assert.assertTrue(o.addParameter(new Parameter("p1", t1)));
		Assert.assertTrue(o.addParameter(new Parameter("p2", t2)));
		Assert.assertTrue(t.addOperation(o));

		o = new Operation<Type>("o2", Modifier.PUBLIC, t);
		Assert.assertTrue(o.addParameter(new Parameter("p1", t1)));
		Assert.assertTrue(o.addParameter(new Parameter("p2", t2)));
		Assert.assertTrue(t.addOperation(o));

		Assert.assertNotNull(t.getOperations());
		Assert.assertEquals(t.getOperations().size(), 5);
		Assert.assertNotNull(t.getOperationsByName("o"));
		Assert.assertEquals(t.getOperationsByName("o").size(), 3);
		
		try {
			t.getOperationByName("o");
			Assert.fail("Exception expected");
		} catch (Exception e) {
			
		}
		
		Assert.assertNotNull(t.getOperationByName("o1"));

		Assert.assertNotNull(t.getOperationByName("o2"));
	}
	
	@Test
	public void addAttributeTwice() {
		Type t = new Type("test", Modifier.PUBLIC);
		
		Assert.assertTrue(t.addAttribute(new Attribute("a", t1, t)));
		Assert.assertFalse(t.addAttribute(new Attribute("a", t1, t)));
	}

	@Test
	public void addAttribute() {
		Type t = new Type("test", Modifier.PUBLIC);
		
		Assert.assertNotNull(t.getAttributes());
		Assert.assertTrue(t.getAttributes().isEmpty());

		Assert.assertTrue(t.addAttribute(new Attribute("a1", t1, t)));
		Assert.assertNotNull(t.getAttributes());
		Assert.assertEquals(t.getAttributes().size(), 1);

		Assert.assertTrue(t.addAttribute(new Attribute("a2", t1, t)));
		Assert.assertEquals(t.getAttributes().size(), 2);

		Assert.assertTrue(t.addAttribute(new Attribute("a3", t1, t)));
		Assert.assertEquals(t.getAttributes().size(), 3);

		Assert.assertTrue(t.addAttribute(new Attribute("a4", t1, t)));
		Assert.assertEquals(t.getAttributes().size(), 4);
	}

	@Test
	public void getAttributeByName() {
		Type t = new Type("test", Modifier.PUBLIC);
		
		Assert.assertNotNull(t.getAttributes());
		Assert.assertTrue(t.getAttributes().isEmpty());

		Assert.assertTrue(t.addAttribute(new Attribute("a1", t1, t)));

		Assert.assertTrue(t.addAttribute(new Attribute("a2", t1, t)));

		Assert.assertTrue(t.addAttribute(new Attribute("a3", t1, t)));

		Assert.assertTrue(t.addAttribute(new Attribute("a4", t1, t)));
		Assert.assertEquals(t.getAttributes().size(), 4);
		
		Assert.assertNotNull(t.getAttributeByName("a1"));
		Assert.assertNotNull(t.getAttributeByName("a4"));
		Assert.assertNull(t.getAttributeByName("a5"));
	}
}
