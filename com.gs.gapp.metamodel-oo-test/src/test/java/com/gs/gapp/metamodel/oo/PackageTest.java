package com.gs.gapp.metamodel.oo;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;

public class PackageTest {

	public static final String NAME = "test";
	
	@Test
	public void constructorName() {
		Package p = new Package(NAME, null);
		
		Assert.assertEquals(p.getName(), NAME);
		Assert.assertNull(p.getOriginatingElement());
		Assert.assertNotNull(p.getOwnedPackages());
		Assert.assertTrue(p.getOwnedPackages().isEmpty());
		Assert.assertNull(p.getOwner());
		Assert.assertTrue(p.isGenerated());
	}

	@Test
	public void constructorNameOwner() {
		Package root = new Package("root", null);
		Package p = new Package(NAME, root);
		
		Assert.assertEquals(p.getName(), NAME);
		Assert.assertNull(p.getOriginatingElement());
		Assert.assertNotNull(p.getOwnedPackages());
		Assert.assertTrue(p.getOwnedPackages().isEmpty());
		Assert.assertNotNull(p.getOwner());
		Assert.assertEquals(p.getOwner(), root);
		Assert.assertTrue(p.isGenerated());
	}

	@Test
	public void constructorOriginNameOwner() {
		Package root = new Package("root", null);
		BigDecimal origin = new BigDecimal(1);
		Package p = new Package(NAME, root);
		p.setOriginatingElement(origin);
		
		Assert.assertEquals(p.getName(), NAME);
		Assert.assertNotNull(p.getOriginatingElement());
		Assert.assertEquals(p.getOriginatingElement(), origin);
		Assert.assertNotNull(p.getOwnedPackages());
		Assert.assertTrue(p.getOwnedPackages().isEmpty());
		Assert.assertNotNull(p.getOwner());
		Assert.assertEquals(p.getOwner(), root);
		Assert.assertTrue(p.isGenerated());
	}

	@Test
	public void isRoot() {
		Package root = new Package("root", null);
		Package p = new Package(NAME, root);
		
		Assert.assertTrue(root.isRoot());
		Assert.assertFalse(p.isRoot());
	}

	@Test
	public void ownedPackages() {
		Package root = new Package("root", null);
		Package p1 = new Package("p1", root);
		Package p2 = new Package("p2", root);
		
		Assert.assertFalse(root.getOwnedPackages().isEmpty());
		Assert.assertEquals(root.getOwnedPackages().size(), 2);
		Assert.assertTrue(root.getOwnedPackages().contains(p1));
		Assert.assertTrue(root.getOwnedPackages().contains(p2));
		
		Assert.assertTrue(p1.getOwnedPackages().isEmpty());
		Assert.assertTrue(p2.getOwnedPackages().isEmpty());
	}

	@Test
	public void hashCodeTest() {
		BigDecimal origin = new BigDecimal(1);
		
		Package root = new Package("root", null);
		root.setOriginatingElement(origin);
		Package p1 = new Package(NAME, root);
		p1.setOriginatingElement(origin);
		int hc = p1.hashCode();
		Assert.assertEquals(p1.hashCode(), hc);
		Assert.assertEquals(p1.hashCode(), hc);
		Assert.assertEquals(p1.hashCode(), hc);

		Package p2 = new Package(NAME, root);
		p2.setOriginatingElement(origin);
		Assert.assertEquals(p1.hashCode(), p2.hashCode());
		
		p2 = new Package(NAME, root);
		Assert.assertEquals(p1.hashCode(), p2.hashCode());
		p2.setGenerated(false);
		Assert.assertEquals(p1.hashCode(), p2.hashCode());
		
		p2 = new Package("abc", root);
		Assert.assertFalse(p1.hashCode() == p2.hashCode());

		p2 = new Package(NAME, p1);
		Assert.assertFalse(p1.hashCode() == p2.hashCode());
	}

	@Test
	public void equalsTest() {
		BigDecimal origin = new BigDecimal(1);
		
		Package root = new Package("root", null);
		root.setOriginatingElement(origin);
		Package p1 = new Package(NAME, root);
		p1.setOriginatingElement(origin);
		Package p2 = new Package(NAME, root);
		p2.setOriginatingElement(origin);
		Assert.assertTrue(p1.equals(p2));
		Assert.assertTrue(p2.equals(p1));
		
		p2 = new Package(NAME, root);
		p2.setOriginatingElement(origin);
		Assert.assertTrue(p1.equals(p2));
		p2.setGenerated(false);
		Assert.assertTrue(p1.equals(p2));
		
		p2 = new Package("abc", root);
		p2.setOriginatingElement(origin);
		Assert.assertFalse(p1.equals(p2));

		p2 = new Package(NAME, p1);
		p2.setOriginatingElement(origin);
		Assert.assertFalse(p1.equals(p2));
	}
	
	@Test
	public void getQualifiedNameTest() {
		Package p1 = new Package("p1", null);
		Package p2 = new Package("p2", p1);
		Package p3 = new Package("p3", p2);
		Assert.assertTrue("package's qualified name is not correct", p3.getQualifiedName(".") != null && p3.getQualifiedName(".").equals("p1.p2.p3"));
		Assert.assertTrue("package's qualified name is not correct", p3.getQualifiedName("/") != null && p3.getQualifiedName("/").equals("p1/p2/p3"));
		Assert.assertTrue("package's qualified name is not correct", p2.getQualifiedName(".") != null && p2.getQualifiedName(".").equals("p1.p2"));
		Assert.assertTrue("package's qualified name is not correct", p1.getQualifiedName(".") != null && p1.getQualifiedName(".").equals("p1"));
	}

}
