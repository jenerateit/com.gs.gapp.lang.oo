package com.gs.gapp.metamodel.oo;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.gs.gapp.metamodel.VisitorState;
import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.ModelElementVisitorBase;
import com.gs.gapp.metamodel.basic.Module;

public class ModelTest {

	// 2
	private Module m1 = null;
	private Module m2 = null;
	
	// 6
	private Package pk11 = null;
	private Package pk12 = null;
	private Package pk111 = null;
	private Package pk112 = null;
	private Package pk121 = null;
	private Package pk122 = null;
	
	// 4
	private BasicClass c111 = null;
	private BasicClass c112 = null;
	private BasicClass c1111 = null;
	private BasicClass c1112 = null;
	
	private Attribute a1111 = null;
	private Attribute a1112 = null;
	private Attribute a1121 = null;
	private Attribute a1122 = null;

	private Operation<BasicClass> o1111 = null;
	private Parameter pa11111 = null;
	private Parameter pa11112 = null;
	private Operation<BasicClass> o1112 = null;
	private Parameter pa11121 = null;
	private Parameter pa11122 = null;
	
	private Package pk21 = null;
	private Package pk22 = null;
	private Package pk211 = null;
	private Package pk212 = null;
	private Package pk221 = null;
	private Package pk222 = null;

	private Set<ModelElement> all = null;
	
	@Before
	public void prepareModel() {
		all = new LinkedHashSet<ModelElement>();
		
		m1 = new Module("m1");
		Assert.assertTrue(all.add(m1));
		
		pk11 = new Package("p11", null);
		Assert.assertTrue(all.add(pk11));
		Assert.assertTrue(m1.addElement(pk11));		

		c111 = new BasicClass("C111", Modifier.PUBLIC, pk11);
		Assert.assertTrue(all.add(c111));
		Assert.assertTrue(pk11.addOwnedType(c111));
		a1111 = new Attribute("a1111", c111, c111);
		Assert.assertTrue(all.add(a1111));
		Assert.assertTrue(c111.addAttribute(a1111));
		a1112 = new Attribute("a1112", c111, c111);
		Assert.assertTrue(all.add(a1112));
		Assert.assertTrue(c111.addAttribute(a1112));

		o1111 = new Operation<BasicClass>("o1111", c111);
		Assert.assertTrue(all.add(o1111));
		pa11111 = new Parameter("pa11111", c111);
		Assert.assertTrue(all.add(pa11111));
		Assert.assertTrue(o1111.addParameter(pa11111));
		pa11112 = new Parameter("pa11112", 0, c111);
		Assert.assertTrue(all.add(pa11112));
		Assert.assertTrue(o1111.addParameter(pa11112));
		o1112 = new Operation<BasicClass>("o1112", c111);
		Assert.assertTrue(all.add(o1112));
		pa11121 = new Parameter("pa11121", 0, c111);
		Assert.assertTrue(all.add(pa11121));
		Assert.assertTrue(o1112.addParameter(pa11121));
		pa11122 = new Parameter("pa11122", 0, c111);
		Assert.assertTrue(all.add(pa11122));
		Assert.assertTrue(o1112.addParameter(pa11122));
		
		c112 = new BasicClass("C112", Modifier.PUBLIC, pk11);
		Assert.assertTrue(all.add(c112));
		Assert.assertTrue(pk11.addOwnedType(c112));
		a1121 = new Attribute("a1121", c112, c112);
		Assert.assertTrue(all.add(a1121));
		Assert.assertTrue(c112.addAttribute(a1121));
		a1122 = new Attribute("a1122", c112, c112);
		Assert.assertTrue(all.add(a1122));
		Assert.assertTrue(c112.addAttribute(a1122));
		
		pk12 = new Package("p12", null);
		Assert.assertTrue(all.add(pk12));
		Assert.assertTrue(m1.addElement(pk12));
		
		pk111 = new Package("p111", pk11);
		Assert.assertTrue(all.add(pk111));

		c1111 = new BasicClass("c1111", Modifier.PUBLIC, pk12);
		Assert.assertTrue(pk111.addOwnedType(c1111));
		Assert.assertTrue(all.add(c1111));
		c1112 =  new BasicClass("c1112", Modifier.PUBLIC, pk12);
		Assert.assertTrue(pk111.addOwnedType(c1112));
		Assert.assertTrue(all.add(c1112));
		pk112 = new Package("p112", pk11);
		Assert.assertTrue(all.add(pk112));
		
		pk121 = new Package("p121", pk12);
		Assert.assertTrue(all.add(pk121));
		pk122 = new Package("p122", pk12);
		Assert.assertTrue(all.add(pk122));
		
		
		m2 = new Module("m2");
		Assert.assertTrue(all.add(m2));

		pk21 = new Package("p21", null);
		Assert.assertTrue(all.add(pk21));
		Assert.assertTrue(m2.addElement(pk21));
		pk22 = new Package("p22", null);
		Assert.assertTrue(all.add(pk22));
		Assert.assertTrue(m2.addElement(pk22));
		
		pk211 = new Package("p211", pk21);
		Assert.assertTrue(all.add(pk211));
		pk212 = new Package("p212", pk21);
		Assert.assertTrue(all.add(pk212));
		
		pk221 = new Package("p221", pk22);
		Assert.assertTrue(all.add(pk221));
		pk222 = new Package("p222", pk22);
		Assert.assertTrue(all.add(pk222));
		
	}
	
	@Test
	public void hashCodeTest() {
		for (ModelElement e : all) {
			e.hashCode();
		}
	}

	@Test
	public void equalsTest() {
		for (ModelElement e : all) {
			Assert.assertTrue(e.equals(e));
		}
	}
	
	private class Visitor extends ModelElementVisitorBase {

		private final AtomicLong counter = new AtomicLong(0L);
		
		@Override
		protected VisitorState visitElement(ModelElement e) {
			counter.incrementAndGet();
			return VisitorState.CONTINUE;
		}
		
		protected long getCounter() {
			return counter.get();
		}
	}
	
	@Test
	public void simpleCountVisit() {
		Visitor visitor  = new Visitor();
		m1.accept(visitor);
		m2.accept(visitor);
		
		Assert.assertEquals(visitor.getCounter(), all.size());
	}
}
