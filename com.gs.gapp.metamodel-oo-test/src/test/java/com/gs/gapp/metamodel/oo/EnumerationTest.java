/**
 * 
 */
package com.gs.gapp.metamodel.oo;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author mmt
 *
 */
public class EnumerationTest {
	
	@Test
	public void getLiteralsTest() {
		Enumeration enumeration = new Enumeration("MyEnum", Modifier.PUBLIC, null);
		EnumerationEntry enumerationEntry1 = new EnumerationEntry("ZZZ");
		enumeration.addEnumerationEntry(enumerationEntry1);
		EnumerationEntry enumerationEntry2 = new EnumerationEntry("YYY");
		enumeration.addEnumerationEntry(enumerationEntry2);
		EnumerationEntry enumerationEntry3 = new EnumerationEntry("XXX");
		enumeration.addEnumerationEntry(enumerationEntry3);
		
		Assert.assertEquals("wrong number of enumeration entries", enumeration.getEntries().size(), 3);
		int ii = 0;
		for (String literal : enumeration.getLiterals()) {
			switch (ii) {
			    case 0:
			        Assert.assertEquals(literal, "XXX", "wrong order of enumeration entries");
			        break;
			    case 1:
			    	Assert.assertEquals(literal, "YYY", "wrong order of enumeration entries");
			        break;
			    case 2:
			    	Assert.assertEquals(literal, "ZZZ", "wrong order of enumeration entries");
			        break;
			}
			ii++;
		}
	}
	
	@Test
	public void getEntriesTest() {
		Enumeration enumeration = new Enumeration("MyEnum", Modifier.PUBLIC, null);
		EnumerationEntry enumerationEntry3 = new EnumerationEntry("E3", 3);
		enumeration.addEnumerationEntry(enumerationEntry3);
		EnumerationEntry enumerationEntry2 = new EnumerationEntry("E2", 2);
		enumeration.addEnumerationEntry(enumerationEntry2);
		EnumerationEntry enumerationEntry1 = new EnumerationEntry("E1", 1);
		enumeration.addEnumerationEntry(enumerationEntry1);
		
		Assert.assertEquals("wrong number of enumeration entries", enumeration.getEntries().size(), 3);
		int ii = 0;
		for (EnumerationEntry entry : enumeration.getEntries()) {
			switch (ii) {
			    case 0:
			        Assert.assertEquals("wrong order of enumeration entries", entry, enumerationEntry1);
			        break;
			    case 1:
			        Assert.assertEquals("wrong order of enumeration entries", entry, enumerationEntry2);
			        break;
			    case 2:
			        Assert.assertEquals("wrong order of enumeration entries", entry, enumerationEntry3);
			        break;
			}
			ii++;
		}
	}

}
