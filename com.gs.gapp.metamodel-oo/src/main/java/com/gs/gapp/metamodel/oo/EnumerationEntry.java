/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.metamodel.oo;

import com.gs.gapp.metamodel.basic.ModelElementI;

/**
 * Represents an enum in the Java language (one EnumerationEntry instance for one instance of an enum definition).
 *
 * @author hrr
 *
 */
public class EnumerationEntry extends Feature<Enumeration>  {

	/*
	 * serial version UID
	 */
	private static final long serialVersionUID = -1940509621895453244L;

	private final Integer ordinal;

	/**
	 * Constructor.
	 *
	 * @param name the name of this element
	 * @param ordinal the ordinal or null if not visible
//	 * @param owner the owning enumeration
	 */
	public EnumerationEntry(String name, Integer ordinal) {//, Enumeration owner) {
		super(name);
//		if (owner != null) {
//			owner.addEnumerationEntry(this);
//		}
		if (ordinal == null) {
			this.ordinal = -1;
		} else {
		    this.ordinal = ordinal;
		}
//		owner.addEnumerationEntry(this);
	}

	/**
	 * @param name the name of this element
	 */
	public EnumerationEntry(String name) {//, Enumeration owner) {
		this(name, -1);
	}

//	/**
//	 * Constructor.
//	 *
//	 * @param name the name of this element
//	 */
//	public EnumerationEntry(String name) {
//		this(name, -1, null);
//	}

	/**
	 * Getter for the ordinal.
	 * <b>NOTE:</b> The ordinal is not part of {@link #hashCode()} and {@link #equals(Object)}.
	 *
	 * @return the ordinal
	 */
	public Integer getOrdinal() {
		return ordinal;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.oo.Feature#compareTo(com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	public int compareTo(ModelElementI element) {
		int result = 0;

		if (element instanceof EnumerationEntry) {
			EnumerationEntry enumerationEntry = (EnumerationEntry) element;
			if (this.ordinal.equals(enumerationEntry.ordinal)) {
				result = super.compareTo(element);
			} else {
				result = this.ordinal.compareTo(enumerationEntry.ordinal);
			}
		} else {
			throw new RuntimeException("ERROR: cannot compare type " + this.getClass().getSimpleName() + " to type " + element.getClass().getSimpleName());
		}

		return result;
	}
}
