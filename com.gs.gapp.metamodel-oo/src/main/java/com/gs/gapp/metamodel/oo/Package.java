/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.metamodel.oo;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.AcceptState;
import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelElementVisitorI;
import com.gs.gapp.metamodel.basic.Module;

/**
 * @author hrr
 *
 */
public class Package extends Feature<ModelElement> {

	/*
	 * serial version UID
	 */
	private static final long serialVersionUID = 2167326479513534623L;

	private final Set<Package> ownedPackages = new LinkedHashSet<Package>();
	private final Set<Type> ownedTypes = new LinkedHashSet<Type>();

	/**
	 * Constructor.
	 *
	 * @param name the name of this element
	 * @param owner the parent package element or Module
	 */
	public Package(String name, ModelElement owner) {
		super(name, Modifier.PUBLIC);

		if (owner != null) {
			setOwner(owner);
			if (Package.class.isInstance(owner)) {
				Package p = Package.class.cast(owner);
				p.ownedPackages.add(this);

			} else if (Module.class.isInstance(owner)) {
				Module m = Module.class.cast(owner);
				m.addElement(this);

			} else {
				throw new RuntimeException("The owner of type '" + owner.getClass().getName() +
						"' is unknown, only Package and Module is supported.");
			}
		}
	}

	/**
	 * Checks if this is the root package.
	 *
	 * @return true if this is the root package otherwise false
	 */
	public boolean isRoot() {
		return getOwner() == null || Module.class.isInstance(getOwner());
	}

	/**
	 * @return the ownedPackages
	 */
	public Set<Package> getOwnedPackages() {
		return ownedPackages;
	}

	/**
	 * Add a type to this package.
	 *
	 * @param t the type to add
	 * @return true if this set did not already contain the specified element
	 */
	public boolean addOwnedType(Type t) {
		t.setOwner(this);
		return this.ownedTypes.add(t);
	}

	/**
	 * @return the ownedTypes
	 */
	public Set<Type> getOwnedTypes() {
		return ownedTypes;
	}

	/**
	 * @param delimiter the delimiter to be used to construct the qualified name of thie package
	 * @return this package's qualified name (e.g. com.gs.gapp for the package "gapp" and the delimiter ".")
	 */
	public String getQualifiedName(String delimiter) {
		if (delimiter == null) { throw new NullPointerException("Parameter delimiter must not be null"); }
		if (delimiter.length() == 0) { throw new IllegalArgumentException("Parameter delimiter has zero length"); }

		StringBuilder result = new StringBuilder(getName());
		Package pkg =
			(Package) (this.getOwner() != null && this.getOwner() instanceof Package ? this.getOwner() : null);

		while (pkg != null) {
			result.insert(0, delimiter).insert(0, pkg.getName());
			pkg = (Package) (pkg.getOwner() != null && pkg.getOwner() instanceof Package ? pkg.getOwner() : null);
		}

		return result.toString();
	}

	/**
	 * Visit all owned packages and types.
	 *
	 * @param visitor the visitor
	 * @return the visitor result
	 * @see com.gs.gapp.metamodel.oo.Feature#visitChilds(com.gs.gapp.metamodel.basic.ModelElementVisitorI)
	 */
	@Override
	protected AcceptState visitChilds(ModelElementVisitorI visitor) {
		if (super.visitChilds(visitor) == AcceptState.CONTINUE) {
			for (Package p : ownedPackages) {
				if (p.accept(visitor) == AcceptState.INTERRUPT) {
					return AcceptState.INTERRUPT;
				}
			}
			for (Type t : ownedTypes) {
				if (t.accept(visitor) == AcceptState.INTERRUPT) {
					return AcceptState.INTERRUPT;
				}
			}
			return AcceptState.CONTINUE;
		}
		return AcceptState.INTERRUPT;
	}

	/**
	 * Compares the owner.
	 *
	 * @param element the element to compare with
	 * @return a negative integer, zero, or a positive integer as this object is less than,
	 * equal to, or greater than the specified object.
	 * @see ModelElement#compareTo(ModelElementI)
	 */
	@Override
	public int compareTo(ModelElementI element) {
		int comp = super.compareTo(element);
		if (Package.class.isInstance(element)) {
			Package p = Package.class.cast(element);
			if (comp == 0) {
				comp = ownedTypes.size() - p.ownedTypes.size();
			}
		}
		return comp;
	}
}
