/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.metamodel.oo;

import com.gs.gapp.metamodel.basic.ParameterType;


/**
 * @author hrr
 *
 */
public class Parameter extends ValueElement<Operation<?>> {

	private static final long serialVersionUID = 7269849629260036643L;

	private final ParameterType parameterType;

	/**
	 * Constructor.
	 *
	 * @param name the name of this element
	 * @param modifier the modifier this attribute has
	 * @param type the type this attribute has
	 * @param parameterType the type of this parameter
	 */
	public Parameter(String name, int modifier, Type type, ParameterType parameterType) {
		super(name, modifier, type, null);

		this.parameterType = parameterType;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Parameter [getType()=" + getType() + ", parameterType="
				+ parameterType + "]";
	}

	/**
	 * Constructor.
	 *
	 * @param name the name of this element
	 * @param modifier the modifier this attribute has
	 * @param type the type this attribute has
	 */
	public Parameter(String name, int modifier, Type type) {
		this(name, modifier, type, ParameterType.IN);
	}

	/**
	 * Constructor.
	 *
	 * @param name the name of this element
	 * @param type the type this attribute has
	 */
	public Parameter(String name, Type type) {
		this(name, Modifier.PRIVATE, type);
	}

	/**
	 * Getter for the parameter type.
	 *
	 * @return the parameterType
	 */
	public ParameterType getParameterType() {
		return parameterType;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((parameterType == null) ? 0 : parameterType.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Parameter other = (Parameter) obj;
		if (parameterType != other.parameterType)
			return false;
		return true;
	}
}
