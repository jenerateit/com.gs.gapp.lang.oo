/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.metamodel.oo;

import java.io.Serializable;

import com.gs.gapp.metamodel.AcceptState;
import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelElementVisitorI;


/**
 * @author hrr
 *
 */
public abstract class ValueElement<T extends ModelElement> extends Feature<T> {


	private Serializable defaultValue;

	/*
	 * serial version UID
	 */
	private static final long serialVersionUID = -8197836632070320707L;

	private final Type type;

	/**
	 * Constructor.
	 *
	 * @param name the name of this element
	 * @param modifier the modifier this attribute has
	 * @param type the type this attribute has
	 */
	public ValueElement(String name, int modifier, Type type, T owner) {
		super(name, modifier, owner);

		if (type == null) {
			throw new IllegalArgumentException("The type of an attribute may not be null");
		}
		this.type = type;
	}

	/**
	 * Constructor. Create a private single attribute.
	 *
	 * @param name the name of this element
	 * @param type the type this attribute has
	 */
	public ValueElement(String name, Type type, T owner) {
		this(name, Modifier.PRIVATE, type, owner);
	}

	/**
	 * Getter for the type this attribute has.
	 *
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

	/**
	 * Getter for the default value.
	 * <b>NOTE</b> This value is not part of the {@link #equals(Object)} and {@link #hashCode()} methods.
	 *
	 * @return the defaultValue
	 */
	public Serializable getDefaultValue() {
		return defaultValue;
	}

	/**
	 * Setter for the default value.
	 * <b>NOTE</b> This value is not part of the {@link #equals(Object)} and {@link #hashCode()} methods.
	 *
	 * @param defaultValue the defaultValue to set
	 */
	public void setDefaultValue(Serializable defaultValue) {
		this.defaultValue = defaultValue;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElement#visitChilds(com.gs.gapp.metamodel.basic.ModelElementVisitorI)
	 */
	@Override
	protected AcceptState visitChilds(ModelElementVisitorI visitor) {
		if (super.visitChilds(visitor) != AcceptState.INTERRUPT) {
			return type.accept(visitor);
		}
		return AcceptState.INTERRUPT;
	}


	/**
	 * Compares the multiplicity and type.
	 *
	 * @param element the element to compare to
	 * @return a negative integer, zero, or a positive integer as this object is less than,
	 * equal to, or greater than the specified object.
	 * @see ModelElement#compareTo(ModelElementI)
	 */
	@Override
	public int compareTo(ModelElementI element) {
		int comp = super.compareTo(element);
		if (ValueElement.class.isInstance(element)) {
			ValueElement<?> ve = ValueElement.class.cast(element);
			if (comp == 0) {
				comp = type.compareTo(ve.type);
			}
		}
		return comp;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + getName().hashCode();
//		result = prime * result + type.hashCode();
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValueElement<?> other = ValueElement.class.cast(obj);
		if (!getName().equals(other.getName()))
			return false;
		if (!type.equals(other.type))
			return false;
		return true;
	}


}
