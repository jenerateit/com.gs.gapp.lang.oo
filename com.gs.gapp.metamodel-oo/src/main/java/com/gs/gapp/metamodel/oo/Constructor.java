/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */
package com.gs.gapp.metamodel.oo;


/**
 * @author hrr
 *
 */
public class Constructor extends Operation<Type> {

	/*
	 * serial version UID
	 */
	private static final long serialVersionUID = 2559987290932842004L;

	/**
	 * @param name
	 * @param modifier
	 */
	public Constructor(String name, int modifier, Type owner) {
		super(name, modifier, owner);
	}

	/**
	 * @param name
	 */
	public Constructor(String name, Type owner) {
		super(name, owner);
	}


}
