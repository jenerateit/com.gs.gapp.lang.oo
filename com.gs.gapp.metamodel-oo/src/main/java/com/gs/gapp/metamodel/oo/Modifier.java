/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.metamodel.oo;

import java.io.Serializable;

/**
 * @author hrr
 *
 */
public class Modifier implements Serializable {

	/*
	 * serial version UID
	 */
	private static final long serialVersionUID = 5377751141334444691L;

	/** The int value representing NO modifier. */
	public static final int NONE 	=		0x0000;
	/** The int value representing the private modifier. */
	public static final int PRIVATE =		0x0001;
	/** The int value representing the protected modifier. */
	public static final int PROTECTED =		0x0002;
	/** The int value representing the public modifier. */
	public static final int PUBLIC =		0x0004;

	/** The int value representing the static modifier. */
	public static final int STATIC = 		0x0010;
	/** The int value representing the final modifier. */
	public static final int FINAL =			0x0020;
	/** The int value representing the abstract modifier. */
	public static final int ABSTRACT = 		0x0040;
	/** The int value representing the virtual modifier. Used in languages like delphi. */
	public static final int VIRTUAL = 		0x0080;

	/**
	 * Calculate the modifier.
	 *
	 * @param modifiers the list of modifiers to use.
	 * @return a representation of all modifiers
	 */
	public static int getModifier(int... modifiers) {
		int modifier = 0x0000;
		for (int m : modifiers) {
			modifier = modifier | m;
		}
		return modifier;
	}

	/**
	 * @param modifierFlags
	 * @param modifierFlagsToAdd
	 * @return
	 */
	public static int addModifier(int modifierFlags, int modifierFlagsToAdd) {
		modifierFlags = modifierFlags | modifierFlagsToAdd;
		return modifierFlags;
	}

	/**
	 * @param modifierFlags
	 * @param modifierFlagsToRemove
	 * @return
	 */
	public static int removeModifier(int modifierFlags, int modifierFlagsToRemove) {
		int m = modifierFlags & ~modifierFlagsToRemove;
		return m;
	}

	/**
	 * Return true if the integer argument includes the private modifier, false otherwise.
	 *
	 * @param modifiers  set of modifiers
	 * @return true if modifiers includes the private modifier; false otherwise.
	 */
	public static boolean isPrivate(int modifiers) {
		return (modifiers & PRIVATE) > 0;
	}

	/**
	 * Return true if the integer argument includes the protected modifier, false otherwise.
	 *
	 * @param modifiers  set of modifiers
	 * @return true if modifiers includes the protected modifier; false otherwise.
	 */
	public static boolean isProtected(int modifiers) {
		return (modifiers & PROTECTED) > 0;
	}

	/**
	 * Return true if the integer argument includes the public modifier, false otherwise.
	 *
	 * @param modifiers  set of modifiers
	 * @return true if modifiers includes the public modifier; false otherwise.
	 */
	public static boolean isPublic(int modifiers) {
		return (modifiers & PUBLIC) > 0;
	}

	/**
	 * Return true if the integer argument includes the static modifier, false otherwise.
	 *
	 * @param modifiers  set of modifiers
	 * @return true if modifiers includes the static modifier; false otherwise.
	 */
	public static boolean isStatic(int modifiers) {
		return (modifiers & STATIC) > 0;
	}

	/**
	 * Return true if the integer argument includes the final modifier, false otherwise.
	 *
	 * @param modifiers  set of modifiers
	 * @return true if modifiers includes the final modifier; false otherwise.
	 */
	public static boolean isFinal(int modifiers) {
		return (modifiers & FINAL) > 0;
	}

	/**
	 * Return true if the integer argument includes the abstract modifier, false otherwise.
	 *
	 * @param modifiers  set of modifiers
	 * @return true if modifiers includes the abstract modifier; false otherwise.
	 */
	public static boolean isAbstract(int modifiers) {
		return (modifiers & ABSTRACT) > 0;
	}


	/**
	 * @param modifier
	 * @return
	 */
	public static int getVisibility(int modifier) {
		if (isPublic(modifier)) {
			return PUBLIC;
		} else if (isProtected(modifier)) {
			return PROTECTED;
		} else if (isPrivate(modifier)) {
			return PRIVATE;
		} else {
			return 0;
		}
	}
}