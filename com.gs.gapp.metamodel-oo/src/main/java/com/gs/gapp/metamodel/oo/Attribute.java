/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.metamodel.oo;


/**
 * @author hrr
 *
 */
public class Attribute extends ValueElement<Type> {

	/*
	 * serial version UID
	 */
	private static final long serialVersionUID = 3889919154922987721L;

	/**
	 * Constructor.
	 *
	 * @param name the name of this element
	 * @param modifier the modifier this attribute has
	 * @param type the type this attribute has
	 */
	public Attribute(String name, int modifier, Type type, Type owner) {
		super(name, modifier, type, owner);
	}

	/**
	 * Constructor.
	 *
	 * @param name the name of this element
	 * @param type the type this attribute has
	 */
	public Attribute(String name, Type type, Type owner) {
		super(name, type, owner);
	}

}
