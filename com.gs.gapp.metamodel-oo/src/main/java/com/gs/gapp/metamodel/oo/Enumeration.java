/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.metamodel.oo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.gs.gapp.metamodel.AcceptState;
import com.gs.gapp.metamodel.basic.ModelElementVisitorI;

/**
 * @author hrr
 *
 */
public class Enumeration extends BasicClass {

	/*
	 * serial version UID
	 */
	private static final long serialVersionUID = 7867870606559855486L;

	private final Set<EnumerationEntry> entries = new LinkedHashSet<EnumerationEntry>();

	/**
	 * Constructor.
	 *
	 * @param name name the name of this type
	 * @param modifier the modifier this feature has
	 */
	public Enumeration(String name, int modifier, Package aPackage) {
		super(name, modifier, aPackage);
	}


	/**
	 * Add an enumeration entry to this enumeration.
	 * <b>NOTE:</b> The entries are not part of {@link #hashCode()} and {@link #equals(Object)}.
	 *
	 * @param entry the entry to add
	 * @return true if this set did not already contain the specified element
	 */
	public boolean addEnumerationEntry(EnumerationEntry entry) {
		if (entry.getOwner() == null) {
		    entry.setOwner(this);
		} else {
			if (entry.getOwner() != this) {
				throw new RuntimeException("ERROR: enumeration entry has wrong owner");
			}
		}
		return this.entries.add(entry);
	}

	/**
	 * Getter for the enumeration entries.
	 * <b>NOTE:</b> The entries are not part of {@link #hashCode()} and {@link #equals(Object)}.
	 *
	 * @return the entries, sorted by the entry ordinal number, and if those are the same, then by the entry names
	 */
	public Set<EnumerationEntry> getEntries() {
		Set<EnumerationEntry> result = new LinkedHashSet<EnumerationEntry>();
		List<EnumerationEntry> tmpList = new ArrayList<EnumerationEntry>();
		tmpList.addAll(this.entries);
		Collections.sort(tmpList);
		result.addAll(tmpList);
		return result;
	}

	/**
	 * Runs the visitor on the enumeration entries.
	 *
	 * @param visitor the visitor
	 * @return the visitor result
	 * @see com.gs.gapp.metamodel.oo.Type#visitChilds(com.gs.gapp.metamodel.basic.ModelElementVisitorI)
	 */
	@Override
	protected AcceptState visitChilds(ModelElementVisitorI visitor) {
		if (super.visitChilds(visitor) != AcceptState.INTERRUPT) {
			for (EnumerationEntry ee : this.entries) {
				if (ee.accept(visitor) == AcceptState.INTERRUPT) {
					return AcceptState.INTERRUPT;
				}
			}
			return AcceptState.CONTINUE;
		}
		return AcceptState.INTERRUPT;
	}

	/**
	 * Returns all enumeration entry names as a list. This may be useful
	 * when you want to implement code-generation for a switch-block.
	 *
	 * @return list of all enumeration entry names, sorted by the entry ordinal number, and if those are the same, then by the entry names
	 */
	public List<String> getLiterals() {
		List<String> result = new ArrayList<String>();
		for (EnumerationEntry enumEntry : getEntries()) {
			result.add(enumEntry.getName());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.oo.BasicClass#addParent(com.gs.gapp.metamodel.oo.BasicClass)
	 */
	@Override
	protected boolean addParent(BasicClass parent) {
        throw new RuntimeException("ERROR: attempt to add a parent class "  + parent + " to the enumeration " + this + ".");
	}



//	/**
//	 * Calculate the hash code.
//	 *
//	 * @return the hash code
//	 * @see java.lang.Object#hashCode()
//	 */
//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = super.hashCode();
//		result = prime * result + entries.hashCode();
//		return result;
//	}
//
//	/**
//	 * Test if this element is equals to the given object.
//	 *
//	 * @param obj the object to test
//	 * @return true if this object is the same as the obj argument; false otherwise
//	 * @see java.lang.Object#equals(java.lang.Object)
//	 */
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (!super.equals(obj))
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		Enumeration other = (Enumeration) obj;
//		if (!entries.equals(other.entries))
//			return false;
//		return true;
//	}


}
