/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.metamodel.oo;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.AcceptState;
import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelElementVisitorI;

/**
 * @author hrr
 *
 */
public class Interface extends Type {

	/*
	 * serial version UID
	 */
	private static final long serialVersionUID = 485605660119606365L;

	private final Set<Interface> parents = new LinkedHashSet<Interface>();


	/**
	 * @param name
	 * @param modifier
	 * @param owner
	 */
	public Interface(String name, int modifier, Package owner) {
		super(name, modifier, owner);
	}


	/**
	 * @return the parents
	 */
	protected Set<Interface> getParents() {
		return parents;
	}

	/**
	 * Add a parent interface to the list of parents.
	 *
	 * @param parent the interface to add
	 * @return true if this set did not already contain the specified element
	 */
	protected boolean addParent(Interface parent) {
		if (parent.equals(this)) {
			throw new RuntimeException("An Interface may not be its own parent");
		}
		return this.parents.add(parent);
	}

	/**
	 * Visit the parent interfaces.
	 *
	 * @param visitor the visitor
	 * @return the visitor result
	 * @see com.gs.gapp.metamodel.oo.Type#visitChilds(com.gs.gapp.metamodel.basic.ModelElementVisitorI)
	 */
	@Override
	protected AcceptState visitChilds(ModelElementVisitorI visitor) {
		if (super.visitChilds(visitor) == AcceptState.INTERRUPT) {
			for (Interface i : this.parents) {
				if (i.accept(visitor) == AcceptState.INTERRUPT) {
					return AcceptState.INTERRUPT;
				}
			}
			return AcceptState.CONTINUE;
		}
		return AcceptState.INTERRUPT;
	}

	/**
	 * Test if the given operation will override an operation in this interface or
	 * its parents.
	 *
	 * @param operation the operation to check for override
	 * @return true if this operation will override an existing operation
	 */
	public boolean isOverride(Operation<?> operation) {
		return getOverride(operation) != null;
	}

	/**
	 * Returns the interface thats operation will be override.
	 *
	 * @param operation the operation to check for override
	 * @return an interface if the operation will be override otherwise null
	 */
	public Interface getOverride(Operation<?> operation) {
		if (getOperations().contains(operation)) {
			return this;
		}
		Interface result = null;
		for (Interface i : this.parents) {
			result = i.getOverride(operation);
			if (result != null) {
				return result;
			}
		}
		return null;
	}

//	/**
//	 * Calculate the hash code.
//	 *
//	 * @return the hash code
//	 * @see java.lang.Object#hashCode()
//	 */
//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = super.hashCode();
//		result = prime * result + parents.hashCode();
//		return result;
//	}
//
//	/**
//	 * Test if this interface is equals the given object.
//	 *
//	 * @param the object to test
//	 * @return true if this object is the same as the obj argument; false otherwise
//	 * @see java.lang.Object#equals(java.lang.Object)
//	 */
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (!super.equals(obj))
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		Interface other = (Interface) obj;
//		if (!parents.equals(other.parents))
//			return false;
//		return true;
//	}

	/**
	 * Compares the parent interfaces.
	 *
	 * @param element the element to compare to
	 * @return a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object.
	 * @see ModelElement#compareTo(ModelElementI)
	 */
	@Override
	public int compareTo(ModelElementI element) {
		int comp = super.compareTo(element);
		if (comp == 0) {
			if (Interface.class.isInstance(element)) {
				Interface i = Interface.class.cast(element);
				comp = this.parents.size() - i.parents.size();
			}
		}
		return comp;
	}


}
