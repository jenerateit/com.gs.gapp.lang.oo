/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.metamodel.oo;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.ModelElementI;

/**
 * @author hrr
 *
 */
public abstract class Feature<T extends ModelElementI> extends ModelElement {

	/*
	 * serial version UID
	 */
	private static final long serialVersionUID = 5745762107703501922L;

	/**
	 * The owner of this Feature.
	 */
	private T owner = null;

	private final int modifier;

	/**
	 * Constructor.
	 *
	 * @param name the name of this element
	 * @param modifier the modifier this feature has
	 */
	public Feature(String name, int modifier) {
		this(name, modifier, null);
	}

	/**
	 * @param name the name of this element
	 * @param modifier the modifier of this feature
	 * @param owner the owner of this feature
	 */
	public Feature(String name, int modifier, T owner) {
		super(name);
		this.modifier = modifier;
		this.owner = owner;
	}

	/**
	 * Constructor.
	 *
	 * @param name the name of this element
	 */
	public Feature(String name) {
		this(name, Modifier.PRIVATE);
	}

	/**
	 * @param name the name of this element
	 * @param owner the owner of this feature
	 */
	public Feature(String name, T owner) {
		this(name, Modifier.PRIVATE, owner);
	}

	/**
	 * Getter for the owner of this feature.
	 *
	 * @return the owner
	 */
	public T getOwner() {
		return owner;
	}

	/**
	 * Setter for the owner of an element.
	 *
	 * @param owner the owner to set
	 */
	public void setOwner(T owner) {
		if (owner == null) {
			throw new NullPointerException("The owner may not be null");
		} else if (this.owner == owner) {
			// reset
		} else if (this.owner == null) {
			this.owner = owner;
		} else {
			throw new RuntimeException("The owner is already set to '" + this.owner + "'");
		}
	}

	/**
	 * Getter for the modifiers of this feature.
	 *
	 * @return the modifier
	 */
	public int getModifier() {
		return modifier;
	}



	/**
	 * Calculates the hash code.
	 *
	 * @return the hash code
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		result = prime * result + modifier;
		return result;
	}

	/**
	 * Checks if this instance is equals obj.
	 *
	 * @param obj the object to check for equals
	 * @return true if this object is the same as the obj argument; false otherwise
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) { return true; }
		if (!super.equals(obj)) { return false; }
		if (getClass() != obj.getClass()) { return false; }

		boolean result = true;
		Feature<?> other = Feature.class.cast(obj);

		if (owner == null) {
			if (other.owner != null) {
				result = false;
			}
		} else if (!owner.equals(other.owner)) {
			result = false;
		}

		if (result == true) {
			// modifier has to be the same, too
			result = this.modifier == other.modifier;
		}

		return result;
	}

	/**
	 * Compares the modifiers and owner.
	 *
	 * @param element the element to compare to
	 * @return a negative integer, zero, or a positive integer as this object is less than,
	 * equal to, or greater than the specified object.
	 * @see ModelElement#compareTo(ModelElementI)
	 */
	@Override
	public int compareTo(ModelElementI element) {
		int comp = super.compareTo(element);

		if (Feature.class.isInstance(element)) {
			Feature<?> f = Feature.class.cast(element);
			if (comp == 0) {
				comp = modifier - f.modifier;
			}
			if (comp == 0) {
				if (owner == null) {
					if (f.owner != null) {
						comp = 1;
					}
				} else {
					comp = owner.compareTo(f.owner);
				}
			}
		}

		return comp;
	}
}
