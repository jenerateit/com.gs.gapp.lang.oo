/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.metamodel.oo;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.AcceptState;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelElementVisitorI;

/**
 * @author hrr
 *
 */
public class Type extends Feature<Package> {

	/*
	 * serial version UID
	 */
	private static final long serialVersionUID = -1613212563438673514L;


	/*
	 * flag to indicate if this type is primitive
	 */
	private final boolean primitive;
	private final Set<Attribute> attributes = new LinkedHashSet<Attribute>();
	private final Set<Operation<?>> operations = new LinkedHashSet<Operation<?>>();


	/**
	 * Constructor.
	 *
	 * @param name the name of this type
	 * @param modifier the modifier this feature has
	 */
	public Type(String name, int modifier) {
		this(name, modifier, false);
	}



	/**
	 * @param name
	 * @param modifier
	 * @param owner
	 */
	public Type(String name, int modifier, Package owner) {
		this(name, modifier, owner, false);
	}

	public Type(String name, int modifier, Package owner, boolean primitive) {
		super(name, modifier, owner);
		this.primitive = primitive;
	}


	/**
	 * @param name
	 * @param owner
	 */
	private Type(String name, Package owner, boolean primitive) {
		super(name, owner);
		this.primitive = primitive;
	}



	/**
	 * Constructor.
	 *
	 * @param name the name of this type
	 * @param modifier the modifier this feature has
	 * @param primitive true if this type is a primitive type otherwise false
	 */
	public Type(String name, int modifier, boolean primitive) {
		super(name, modifier);
		this.primitive = primitive;
	}

	/**
	 * Constructor.
	 *
	 * @param name the name of this type
	 * @param primitive true if this type is a primitive type otherwise false
	 */
	public Type(String name, boolean primitive) {
		this(name, Modifier.PUBLIC, primitive);
	}

	/**
	 * Constructor.
	 *
	 * @param name the name of this type
	 */
	public Type(String name) {
		this(name, Modifier.PUBLIC, false);
	}

	/**
	 * Getter for the primitive flag.
	 *
	 * @return the primitive flag
	 */
	public boolean isPrimitive() {
		return primitive;
	}

	/**
	 * Getter for a list of Attributes.
	 *
	 * @return the attributes
	 */
	protected Set<Attribute> getAttributes() {
		return attributes;
	}

	/**
	 * Returns the Attribute with the given name.
	 *
	 * @param name the name to search for
	 * @return an operation
	 */
	protected Attribute getAttributeByName(String name) {
		for (Attribute a : getAttributes()) {
			if (a.getName().compareTo(name) == 0) {
				return a;
			}
		}
		return null;
	}

	/**
	 * Add an Attribute to the list of attributes.
	 *
	 * @param attribute the attribute to add
	 * @return true if this set did not already contain the specified element
	 */
	protected boolean addAttribute(Attribute attribute) {
		if (isPrimitive()) {
			throw new RuntimeException("The type '" + getName() + "' is primitive and does not have attributes");

		} else if (this.attributes.add(attribute)) {
			attribute.setOwner(this);
			return true;
		}
		return false;
	}


	/**
	 * Getter for the operations of this type.
	 *
	 * @return the operations
	 */
	protected Set<Operation<?>> getOperations() {
		return operations;
	}

	/**
	 * Return an Operation with the given name.
	 *
	 * @param name the name to search for
	 * @return an operation
	 */
	protected Operation<?> getOperationByName(String name) {
		Set<Operation<?>> namedOperations = getOperationsByName(name);
		if (namedOperations.isEmpty()) {
			return null;
		} else if (namedOperations.size() > 1) {
			throw new RuntimeException("Found more than one Operation with name '" + name +
					"' in type '" + getName() + "'");
		}
		return namedOperations.iterator().next();
	}

	/**
	 * Returns all Operations with the given name.
	 *
	 * @param name the name to search for
	 * @return all operations with the given name
	 */
	protected Set<Operation<?>> getOperationsByName(String name) {
		Set<Operation<?>> namedOperations = new LinkedHashSet<Operation<?>>();
		for (Operation<?> o : getOperations()) {
			if (o.getName().compareTo(name) == 0) {
				namedOperations.add(o);
			}
		}

		return namedOperations;
	}

	/**
	 * Method to search for Constructors.
	 *
	 * @return the constructors
	 */
	protected Set<Constructor> getConstructors() {
		Set<Constructor> constructors = new LinkedHashSet<Constructor>();
		for (Operation<?> o : getOperations()) {
			if (Constructor.class.isInstance(o)) {
				constructors.add(Constructor.class.cast(o));
			}
		}
		return constructors;
	}

	/**
	 * Method to search for Destructors.
	 *
	 * @return the destructors
	 */
	protected Set<Destructor> getDestructors() {
		Set<Destructor> destructors = new LinkedHashSet<Destructor>();
		for (Operation<?> o : getOperations()) {
			if (Destructor.class.isInstance(o)) {
				destructors.add(Destructor.class.cast(o));
			}
		}
		return destructors;
	}

	/**
	 * Add an Operation to the list of operations.
	 *
	 * @param operation the operation to add
	 * @return true if this set did not already contain the specified element
	 */
	protected boolean addOperation(Operation<Type> operation) {
		if (isPrimitive()) {
			throw new RuntimeException("The type '" + getName() + "' is primitive and does not have operations");

		} else if (this.operations.add(operation)) {
			operation.setOwner(this);
			return true;
		}
		return false;
	}


	/**
	 * Accept the visitor on all attributes and operations.
	 *
	 * @param visitor the visitor
	 * @return the visitor result
	 * @see com.gs.gapp.metamodel.oo.Feature#visitChilds(com.gs.gapp.metamodel.basic.ModelElementVisitorI)
	 */
	@Override
	protected AcceptState visitChilds(ModelElementVisitorI visitor) {
		if (super.visitChilds(visitor) != AcceptState.INTERRUPT) {
			for (Attribute a : getAttributes()) {
				if (a.accept(visitor) == AcceptState.INTERRUPT) {
					return AcceptState.INTERRUPT;
				}
			}
			for (Operation<?> o : getOperations()) {
				if (o.accept(visitor) == AcceptState.INTERRUPT) {
					return AcceptState.INTERRUPT;
				}
			}
			return AcceptState.CONTINUE;
		}
		return AcceptState.INTERRUPT;
	}

	/**
	 * Calculate a hash code.
	 *
	 * @return the hash code
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (primitive ? 1231 : 1237);
		return result;
	}

	/**
	 * Check this type with the given obj for equality.
	 *
	 * @param obj the object to check
	 * @return true if this object is the same as the obj argument; false otherwise
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) { return true; }
		if (!super.equals(obj)) { return false; }
		if (getClass() != obj.getClass()) { return false; }

		Type other = (Type) obj;
		if (primitive != other.primitive) {
			return false;
		}

		return true;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.oo.Feature#compareTo(com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	public int compareTo(ModelElementI element) {
		int comp = super.compareTo(element);
		if (comp == 0 && Type.class.isInstance(element)) {
			Type t = Type.class.cast(element);
			comp = this.attributes.size() - t.attributes.size();
			if (comp == 0) {
				comp = this.operations.size() - t.operations.size();
			}
		}
		return comp;
	}

}
