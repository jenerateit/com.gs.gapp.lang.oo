/**
 * 
 */
package com.gs.gapp.metamodel.oo;

/**
 * @author hrr
 *
 */
public interface Constants {

	public static final Type CHAR = new Type("char", true);
	public static final Type STRING = new Type("string", true);
	
	public static final Type BYTE = new Type("byte", true);
	public static final Type SHORT = new Type("short", true);
	
	public static final Type INTEGER = new Type("integer", true);
	public static final Type LONG = new Type("long", true);
	public static final Type BOOLEAN = new Type("boolean", true);
	
	public static final Type FLOAT = new Type("float", true);
	public static final Type DOUBLE = new Type("double", true);
	
	public static final Type VOID = new Type("void", true);
}
