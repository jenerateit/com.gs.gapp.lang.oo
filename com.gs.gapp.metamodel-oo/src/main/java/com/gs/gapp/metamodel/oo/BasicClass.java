/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.metamodel.oo;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.AcceptState;
import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelElementVisitorI;

/**
 * @author hrr
 *
 */
public class BasicClass extends Type {

	/*
	 * serial version UID
	 */
	private static final long serialVersionUID = -2239694841265059239L;

	private final Set<BasicClass> parents = new LinkedHashSet<BasicClass>();
	private final Set<Interface> interfaces = new LinkedHashSet<Interface>();
	private final Set<BasicClass> innerClasses = new LinkedHashSet<BasicClass>();

	/**
	 * Constructor.
	 *
	 * @param name the name of this type
	 * @param modifier the modifier this feature has
	 */
	public BasicClass(String name, int modifier, Package aPackage) {
		super(name, modifier, aPackage, false);
	}

	/**
	 * Add a parent to the list of parents.
	 *
	 * @param parent the parent to add
	 * @return true if this set did not already contain the specified element
	 */
	protected boolean addParent(BasicClass parent) {
		if (parent.equals(this)) {
			throw new RuntimeException("An BasicClass may not be its own parent");
		}
		return this.parents.add(parent);
	}

	/**
	 * Getter for the parent list.
	 *
	 * @return the parents
	 */
	protected Set<BasicClass> getParents() {
		return parents;
	}

	/**
	 * Add an interface to the list of interfaces.
	 *
	 * @param interf the interface to add
	 * @return true if this set did not already contain the specified element
	 */
	protected boolean addInterface(Interface interf) {
		return this.interfaces.add(interf);
	}

	/**
	 * Getter for the interfaces this class implements.
	 *
	 * @return the interfaces
	 */
	protected Set<Interface> getInterfaces() {
		return interfaces;
	}

	/**
	 * Add an inner class to this class.
	 *
	 * @param innerClass the inner class to add
	 * @return true if this set did not already contain the specified element
	 */
	protected boolean addInnerClass(BasicClass innerClass) {
		return this.innerClasses.add(innerClass);
	}

	/**
	 * Getter for the inner classes this class has.
	 *
	 * @return the innerClasses
	 */
	protected Set<BasicClass> getInnerClasses() {
		return innerClasses;
	}

	/**
	 * Visit the parent classes, the interfaces and inner classes.
	 *
	 * @param visitor the visitor
	 * @return the visitor result
	 * @see Type#visitChilds(ModelElementVisitorI)
	 */
	@Override
	protected AcceptState visitChilds(ModelElementVisitorI visitor) {
		if (super.visitChilds(visitor) != AcceptState.INTERRUPT) {
			for (BasicClass c : this.parents) {
				if (c.accept(visitor) == AcceptState.INTERRUPT) {
					return AcceptState.INTERRUPT;
				}
			}
			for (Interface i : this.interfaces) {
				if (i.accept(visitor) == AcceptState.INTERRUPT) {
					return AcceptState.INTERRUPT;
				}
			}
			for (BasicClass c : this.innerClasses) {
				if (c.accept(visitor) == AcceptState.INTERRUPT) {
					return AcceptState.INTERRUPT;
				}
			}
			return AcceptState.CONTINUE;
		}
		return AcceptState.INTERRUPT;
	}

	/**
	 * Test if the given operation will override an operation in this class, it's interfaces and
	 * its parent classes.
	 *
	 * @param operation the operation to check for override
	 * @return true if this operation will override an existing operation
	 */
	public boolean isOverride(Operation<?> operation) {
		return getOverride(operation) != null;
	}

	/**
	 * Returns the type that's operation will be override. All operations in this class,
	 * it's interfaces and its parent classes will be checked.
	 *
	 * @param operation the operation to check for override
	 * @return an type if the operation will be override otherwise null
	 */
	public Type getOverride(Operation<?> operation) {
		if (getOperations().contains(operation)) {
			return this;
		}
		Type result = null;
		for (Interface i : this.interfaces) {
			result = i.getOverride(operation);
			if (result != null) {
				return result;
			}
		}
		for (BasicClass c : this.parents) {
			result = c.getOverride(operation);
			if (result != null) {
				return result;
			}
		}
		return null;
	}

	//	/**
//	 * Calculate a hash code.
//	 *
//	 * @return the hash code
//	 * @see java.lang.Object#hashCode()
//	 */
//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = super.hashCode();
//		result = prime * result	+ innerClasses.hashCode();
//		result = prime * result	+ interfaces.hashCode();
//		result = prime * result + parents.hashCode();
//		return result;
//	}
//
//	/**
//	 * Check if the given object is equals this element.
//	 *
//	 * @param obj the object to test with
//	 * @return true if this object is the same as the obj argument; false otherwise
//	 * @see java.lang.Object#equals(java.lang.Object)
//	 */
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (!super.equals(obj))
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		Class other = (Class) obj;
//		if (!innerClasses.equals(other.innerClasses))
//			return false;
//		if (!interfaces.equals(other.interfaces))
//			return false;
//		if (!parents.equals(other.parents))
//			return false;
//		return true;
//	}

	/**
	 * Compares the inner classes, interfaces and parents.
	 *
	 * @param element the element to compare to
	 * @return a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object.
	 * @see ModelElement#compareTo(ModelElementI)
	 */
	@Override
	public int compareTo(ModelElementI element) {
		int comp = super.compareTo(element);
		if (BasicClass.class.isInstance(element)) {
			BasicClass c = BasicClass.class.cast(element);
			if (comp == 0) {
				comp = this.innerClasses.size() - c.innerClasses.size();
			}

			if (comp == 0) {
				comp = this.interfaces.size() - c.interfaces.size();
			}

			if (comp == 0) {
				comp = this.parents.size() - c.parents.size();
			}
		}
		return comp;
	}
	
}
