/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */
package com.gs.gapp.metamodel.oo;


/**
 * @author hrr
 *
 */
public class Destructor extends Operation<Type> {

	/*
	 * serial version UID
	 */
	private static final long serialVersionUID = -9167021476624990746L;

	/**
	 * @param name
	 * @param modifier
	 */
	public Destructor(String name, int modifier, Type owner) {
		super(name, modifier, owner);
	}

	/**
	 * @param name
	 */
	public Destructor(String name, Type owner) {
		super(name, owner);
	}

	@Override
	public boolean addParameter(Parameter parameter) {
		throw new RuntimeException("A Destructor must not have parameters");
	}

	@Override
	public boolean addException(BasicClass exc) {
		throw new RuntimeException("A Destructor must not throw an Exception");
	}

}
