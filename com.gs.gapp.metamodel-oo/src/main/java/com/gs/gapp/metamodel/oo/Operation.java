/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.metamodel.oo;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.AcceptState;
import com.gs.gapp.metamodel.basic.ModelElementVisitorI;
import com.gs.gapp.metamodel.basic.ParameterType;

/**
 * @author hrr
 *
 */
public class Operation<T extends Type> extends Feature<T> {

	/*
	 * serial version UID
	 */
	private static final long serialVersionUID = -1958861308018060009L;

	private final Set<Parameter> parameters = new LinkedHashSet<Parameter>();
	private final Set<BasicClass> exceptions = new LinkedHashSet<BasicClass>();

	/**
	 * @param name
	 * @param modifier
	 * @param owner
	 */
	public Operation(String name, int modifier, T owner) {
		super(name, modifier, owner);
	}


	/**
	 * @param name
	 * @param owner
	 */
	public Operation(String name, T owner) {
		super(name, owner);
	}

	/**
	 * Getter for all parameters.
	 *
	 * @return the parameters
	 */
	public Set<Parameter> getParameters() {
		return parameters;
	}


	/**
	 * @param position
	 * @return
	 */
	public Parameter getInParameter(int position) {
		Set<Parameter> inParameters = getInParameters();

		if (position >= 0 && position < inParameters.size()) {
			return inParameters.toArray(new Parameter[0])[position];
		}

		return null;
	}

	/**
	 * Add a parameter to this operation.
	 *
	 * @param parameter the parameter to add
	 * @return true if the operation's set of parameters did not already contain the specified element
	 */
	protected boolean addParameter(Parameter parameter) {
		if (parameter == null) throw new NullPointerException("parameter 'parameter' must not be null");
		parameter.setOwner(this);
		boolean result = this.parameters.add(parameter);
		if (result == false) {
			System.out.println("operation '" + this.getName() +"' did already contain the parameter '" + parameter.getName() + "'");
		}
		return result;
	}

	/**
	 * Getter for all parameters with a given parameter type.
	 *
	 * @param type the filter type
	 * @return the parameter list
	 */
	public Set<Parameter> getParameterByType(ParameterType type) {
		Set<Parameter> result = new LinkedHashSet<Parameter>();
		for (Parameter p : this.parameters) {
			if (p.getParameterType() == type) {
				result.add(p);
			}
		}
		return result;
	}

	/**
	 * @return
	 */
	public Set<Parameter> getInParameters() {
		Set<Parameter> result = new LinkedHashSet<Parameter>();
		for (Parameter p : this.parameters) {
			if (p.getParameterType() != ParameterType.OUT) {
				result.add(p);
			}
		}
		return result;
	}

	/**
	 * Getter for the exceptions.
	 *
	 * @return the exceptions
	 */
	public Set<BasicClass> getExceptions() {
		return exceptions;
	}

	/**
	 * Add an exception to this operation.
	 *
	 * @param exc the exception to add
	 * @return true if this set did not already contain the specified element
	 */
	public boolean addException(BasicClass exc) {
		return this.exceptions.add(exc);
	}

	/**
	 * @return the operation's return parameter, null if there is none
	 * @throws RuntimeException when there exists more than one parameter of type ParameterType.OUT
	 */
	protected Parameter getReturnParameter() {
//		Parameter result = null;
		Set<Parameter> outs = getParameterByType(ParameterType.OUT);
//		for (Parameter parameter : getParameters()) {
//			if (parameter.getParameterType() == ParameterType.OUT) {
//				if (result != null) {
		if (outs.isEmpty()) {
			return null;
		} else if (outs.size() > 1) {
					throw new RuntimeException("ERROR: Operation must not have more than one return parameter (operation name '" + this.getName() + "')");
				} else {
					return outs.iterator().next();
				}
//				result = parameter;
//			}
//		}
//
//		return result;
	}

	/**
	 * Getter for the Type of the parameter with type {@link ParameterType#OUT}.
	 *
	 * @return the operation's return type, null if there is no return parameter defined for this operation
	 */
	protected Type getReturnType() {
		Parameter returnParameter = getReturnParameter();
		if (returnParameter != null) {
			return returnParameter.getType();
		} else {
			return null;
		}
	}

	/**
	 * Visit the return type (if available) the parameters and exceptions.
	 *
	 * @param visitor the visitor
	 * @return the visitor result
	 * @see Feature#visitChilds(ModelElementVisitorI)
	 */
	@Override
	protected AcceptState visitChilds(ModelElementVisitorI visitor) {
		if (super.visitChilds(visitor) != AcceptState.INTERRUPT) {
			for (Parameter p : this.parameters) {
				if (p.accept(visitor) == AcceptState.INTERRUPT) {
					return AcceptState.INTERRUPT;
				}
			}
			for (BasicClass e : this.exceptions) {
				if (e.accept(visitor) == AcceptState.INTERRUPT) {
					return AcceptState.INTERRUPT;
				}
			}
			return AcceptState.CONTINUE;
		}
		return AcceptState.INTERRUPT;
	}

	/**
	 * Calculates the hash code.
	 *
	 * @return the hash code
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + getName().hashCode();
//		result = prime * result	+ exceptions.hashCode();
		if (this.parameters != null) {
			for (Parameter p : this.parameters) {
				result = prime * result	+ p.getType().hashCode();
			}
		}
//		result = prime * result	+ ((returnType == null) ? 0 : returnType.hashCode());
		return result;
	}

	/**
	 * Checks if the obj is equals to this implementation.
	 *
	 * @param obj the object to compare to
	 * @return true if this instance is equals to obj otherwise false
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Operation<?> other = Operation.class.cast(obj);
//		if (!exceptions.equals(other.exceptions))
//			return false;
		if (!getName().equals(other.getName()))
			return false;

		if (parameters != null && other.parameters != null) {
			// TODO this is a loop, is it?
			if (parameters.size() != other.parameters.size())
				return false;
			Iterator<Parameter> i1 = parameters.iterator();
			Iterator<Parameter> i2 = parameters.iterator();
			while (i1.hasNext() && i2.hasNext()) {
				if (!i1.next().getType().equals(i2.next().getType())) {
					return false;
				}
			}
		}
		return true;
	}


}
