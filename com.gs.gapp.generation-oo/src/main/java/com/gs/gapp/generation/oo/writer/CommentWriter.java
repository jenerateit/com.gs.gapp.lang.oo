/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 *
 */
package com.gs.gapp.generation.oo.writer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.jenerateit.JenerateITI;
import org.jenerateit.target.TargetI;
import org.jenerateit.target.TargetSection;
import org.jenerateit.target.TextTargetI;
import org.jenerateit.util.StringTools;
import org.jenerateit.writer.AbstractTextWriter;
import org.jenerateit.writer.WriterException;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.basic.AbstractGenerationGroup;
import com.gs.gapp.metamodel.basic.ModelElementCache;

/**
 * Implementation of a {@link WriterI} for text based documents.
 * This writer is responsible to write comments.
 *
 * @author hrr
 * @deprecated moved comment writing methods to ModelElementWriter class in com.gs.gapp.generation-basic
 */
@Deprecated
public abstract class CommentWriter extends AbstractTextWriter {

	/**
	 * Creates a quoted String.
	 *
	 * @param value the string to quote
	 */
	protected String getQuotedString(String value) {
		return new StringBuilder("\"").append(value).append("\"").toString();
	}
	/**
	 * Helper method to parse text segments for line breaks.
	 * The text segment will be split into separate lines.
	 *
	 * @see BufferedReader#readLine()
	 * @param text the text segment to parse
	 * @return an array of lines
	 */
	protected CharSequence[] getLines(CharSequence text) {
		// TODO this is content that could be moved into the StringTools
		if (StringTools.isNotEmpty(text.toString())) {
			List<String> lines = new ArrayList<String>();
			BufferedReader br = new BufferedReader(new StringReader(text.toString()));
			String s = null;
			try {
				while ((s = br.readLine()) != null) {
					lines.add(s);
				}

			} catch (IOException e) {
				throw new WriterException("Error while parse a text segment for line breaks",
						e, getTransformationTarget(), this);
			}

			return lines.toArray(new String[lines.size()]);
		}
		return new String[] {};
	}

	/**
	 * Write comment(s) to the current working target section
	 * <b>Note:</b> The comment character will be added in front and the end of a line.
	 * If the line has line separators each line will be added separately with comment signs.
	 *
	 * @param comments the comment(s) to write
	 * @see #wComment(TargetSection, CharSequence...)
	 * @return an instance of this writer object
	 */
	public CommentWriter wComment(String... comments) {
		return wComment(null, comments);
	}

	/**
	 * Write comment(s) to the given target section ts.
	 * If ts is null the current working target section is used to write to.
	 * <b>Note:</b> The comment character will be added in front and the end of a line.
	 * If the line has line separators each line will be added separately with comment signs.
	 *
	 * @param comments the comment(s) to write
	 * @param ts the target section to write to
	 * @return an instance of this writer object
	 */
	public CommentWriter wComment(TargetSection ts, CharSequence... comments) {
		if (comments == null || comments.length == 0) {
//			logger.warn("Found a wComment(TargetSection, String...) method call but no comments are there");

		} else {
			TargetI<?> t = getTransformationTarget();
			if (TextTargetI.class.isInstance(t)) {
				TextTargetI<?> tt = (TextTargetI<?>) t;
				for (CharSequence c : comments) {
					for (CharSequence s : getLines(c)) {
						if (ts != null) {
							wNL(ts, tt.getCommentStart(), " ", s, " ", tt.getCommentEnd());
						} else {
							wNL(tt.getCommentStart(), " ", s, " ", tt.getCommentEnd());
						}
					}
				}
			} else {
				throw new WriterException("Can not write a comment in a Target of type '" + t.getClass().getName() + "'", t, this);
			}
		}
		return this;
	}

	/**
	 * Writes the {@link Object#toString() objects} in the output file as a comment
	 * if the {@link JenerateITI#isDevelopmentMode() development mode} of the corresponding
	 * {@link JenerateITI jenerateit} is switched on.
	 *
	 * @param objs the objects to write
	 * @see Object#toString()
	 * @see JenerateITI#isDevelopmentMode()
	 * @return an instance of this writer object
	 */
	public CommentWriter wDebug(Object... objs) {
		final JenerateITI jenerateIT = getTransformationTarget().getGenerationGroup().getTargetProject().getJenerateIT();
		if (jenerateIT != null && jenerateIT.isDevelopmentMode()) {
			for (Object o : objs) {
				wComment(o != null ? o.toString() : "null");
			}
		}
		return this;
	}

	/**
	 * @return
	 */
	public ModelElementCache getModelElementCache() {
		if (getTransformationTarget() != null) {
			if (getTransformationTarget().getGenerationGroup().getConfig() != null) {
				if (getTransformationTarget().getGenerationGroup().getConfig() instanceof AbstractGenerationGroup) {
					return ((AbstractGenerationGroup)getTransformationTarget().getGenerationGroup().getConfig()).getModelElementCache();
				}
			}
		}
		return null;
	}
}
